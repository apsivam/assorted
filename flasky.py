from flask import Flask
from flask_admin import Admin
from csvdb import CSVModelView, CSVDataModel, CSVProcessor

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret123'
app.config['FLASK_ADMIN_SWATCH'] = 'flatly'

authorCSV = CSVProcessor(csvfile='test.csv')
admin = Admin(app, template_mode='bootstrap3')


class Author(CSVDataModel):
    backend = authorCSV


class AuthorView(CSVModelView):
    can_view_details = True
    can_delete = False
    column_exclude_list = [ 'id' ]


admin.add_view(AuthorView(Author))


@app.route('/')
def index():
    return '<a href="/admin/">Click me to get to Admin!</a>'


if __name__ == '__main__':
    app.run(debug=True)
