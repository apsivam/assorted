import csv
from flask_admin.model import BaseModelView
from flask_admin import form
from wtforms.form import Form
from wtforms import StringField
import pprint

class CSVProcessor(object):
    def __init__(self, csvfile, separator=',', fieldnames=('c1', 'c2', 'c3'), *args, **kwargs):
        self.csvfile = csvfile
        self.separator = separator
        self.fieldnames = fieldnames
        self.csvdata = self._readcsv()

    def _readcsv(self):
        csvreader = csv.DictReader(open(self.csvfile, 'r'), fieldnames=self.fieldnames)
        csvdata = []
        for row in csvreader:
            csvdata.append({'id': csvreader.line_num, **dict(row)})
        return csvdata

    def get_data(self):
        return self.csvdata

    def get_columns(self):
        return list(self.csvdata[0].keys())

    def update_row(self, data_obj):
        data_dict = data_obj.__dict__
        row_no = data_dict['id']
        del data_dict['id']
        csvreader = csv.DictReader(open(self.csvfile, 'r'), fieldnames=self.fieldnames)
        csvwriter = csv.DictWriter(open("{0}_".format(self.csvfile), 'w'), fieldnames=self.fieldnames, extrasaction="ignore")
        for _ in csvreader:
            if csvreader.line_num == row_no:
                csvwriter.writerow(data_dict)
        return True

class CSVDataModel(object):
    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __getitem__(self, item):
        return self.__dict__.get(item)


class CSVModelView(BaseModelView):
    def __init__(self, model, *args, **kwargs):
        super().__init__(model, *args, **kwargs)

    def get_pk_value(self, model):
        return model['id']

    def scaffold_list_columns(self):
        return self.model.backend.get_columns()

    def scaffold_sortable_columns(self):
        return None

    def init_search(self):
        return False

    def scaffold_form(self):
        class MyForm(Form):
            c1 = StringField('c1')
            c2 = StringField('c2')
            c3 = StringField('c3')
        return MyForm

    def get_list(self, page, sort_field, sort_desc, search, filters, page_size=None):
        csvdata = self.model.backend.get_data()
        return len(csvdata), [self.model(**d) for d in csvdata]

    def get_one(self, id):
        csvdata = self.model.backend.get_data()
        return self.model(**(next(item for item in csvdata if item['id'] == int(id))))

    def update_model(self, form, model):
        form.populate_obj(model)
        self.model.backend.update_row(model)
        return True